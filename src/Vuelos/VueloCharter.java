package Vuelos;

import java.time.LocalDate;

public class VueloCharter extends Vuelo {
	
	public VueloCharter(String nombre, LocalDate fechaDeVuelo, String origen, String destino,
			double kilometrosPorRecorrer,int pasajerosAsignados,String tipo) {
		super(nombre, fechaDeVuelo, origen, destino, kilometrosPorRecorrer, tipo);
		this.pasajerosAignado=pasajerosAsignados;
	}
	

	
	private int pasajerosAignado;
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public double pesoDeLacarga() {
		return 5000;
	}

	@Override
	public double pesoDePasajeros() {
		return this.avion.getCantidadDeAsientos() - this.getCantidadDeAsientosComprados(); 
	}

	@Override
	public void pesoPermitidoDeEmpresas(double peso) {
		this.pesoPermitidoDeLaEmpresa=peso;
		
	}

	@Override
	public int getCantidadDeAsientos() {
		return this.avion.getCantidadDeAsientos();
	}

	@Override
	public int getCantidadDeAsientosLibres() {
		
			return (((this.avion.getCantidadDeAsientos() - this.getCantidadDeAsientosComprados())-this.pasajerosAignado)-25); 
		
	}

}
