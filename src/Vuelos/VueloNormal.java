package Vuelos;

import java.time.LocalDate;

import Aerolinea.Aerolines.Avion;
import Aerolinea.Aerolines.Iata;
import politica.Politica;


public class VueloNormal extends Vuelo {
	
	public VueloNormal(String nombre, LocalDate fechaDeVuelo, String origen, String destino, 
			double kilometrosPorRecorrer,String tipo) {
		super(nombre, fechaDeVuelo, origen, destino, kilometrosPorRecorrer, tipo);
	}


	public void pesoPermitidoDeEmpresas(double peso){
		this.pesoPermitidoDeLaEmpresa= peso;
	}


	public double importeTotal(){
	return
			this.pasajes.stream().mapToDouble (p -> p.getPrecio() ).sum() ;
	}



	public Politica getPolitica() {
		return politica;
	}
	
	public void setPolitica(Politica politica) {
		this.politica = politica;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}
	public boolean esRelajado() {
		return ((avion.getCantidadDeAsientos() < 100) && (avion.getAlturaCabina()> 4));
	}

	public Iata getIata() {
		return iata;
	}
	public double pesoDePasajeros (){
		return this.getCantidadDeAsientosComprados() * VueloNormal.iata.getPeso();
	}
	@Override
	public double pesoDeLacarga() {
		return this.getCantidadDeAsientosComprados() * this.pesoPermitidoDeLaEmpresa;
	}


	@Override
	public int getCantidadDeAsientos() {
			return this.avion.getCantidadDeAsientos();
	}


	@Override
	public int getCantidadDeAsientosLibres() {
		
			return this.avion.getCantidadDeAsientos() - this.getCantidadDeAsientosComprados(); 
		}


	
}
