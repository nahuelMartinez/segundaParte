package Vuelos;

import java.time.LocalDate;

import java.util.Collection;
import java.util.HashSet;


import Aerolinea.Aerolines.Avion;
import Aerolinea.Aerolines.Iata;
import Aerolinea.Aerolines.Pasaje;
import criterioEmpresa.CriterioDeVuelo;
import criterioEmpresa.Empresa;
import politica.Politica;
import org.uqbar.commons.utils.Observable;


@Observable
public abstract class Vuelo {
	
	protected String nombre;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Avion getAvion() {
		return avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Politica getPolitica() {
		return politica;
	}

	public void setPolitica(Politica politica) {
		this.politica = politica;
	}

	public double getPesoPermitidoDeLaEmpresa() {
		return pesoPermitidoDeLaEmpresa;
	}

	public void setPesoPermitidoDeLaEmpresa(double pesoPermitidoDeLaEmpresa) {
		this.pesoPermitidoDeLaEmpresa = pesoPermitidoDeLaEmpresa;
	}

	public double getKilometrosPorRecorrer() {
		return kilometrosPorRecorrer;
	}

	public void setKilometrosPorRecorrer(double kilometrosPorRecorrer) {
		this.kilometrosPorRecorrer = kilometrosPorRecorrer;
	}

	public LocalDate getFechaDeVuelo() {
		return fechaDeVuelo;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public void setPasajes(Collection<Pasaje> pasajes) {
		this.pasajes = pasajes;
	}


	protected static Iata iata;
	protected Avion avion;
	protected LocalDate fechaDeVuelo;
	protected String origen;
	protected String destino;
	protected Politica politica;
	protected double precioEstandar;
	protected double pesoPermitidoDeLaEmpresa;
	protected double kilometrosPorRecorrer;
	protected String tipo;
	protected CriterioDeVuelo criterio;
	protected Empresa empresa;
	
	
	
	public boolean sePuedeVender(){
		return this.criterio.puedeVender(this);
	}
	
	public CriterioDeVuelo getCriterio() {
		return criterio;
	}

	public void setCriterio(CriterioDeVuelo criterio) {
		this.criterio = criterio;
	}


	public Collection<Pasaje> pasajes = new HashSet<>();
	
	
public Vuelo(String nombre, LocalDate fechaDeVuelo, String origen, String destino,
			double kilometrosPorRecorrer,String tipo) {
	
		this.nombre = nombre;
		this.fechaDeVuelo = fechaDeVuelo;
		this.origen = origen;
		this.destino = destino;
		this.kilometrosPorRecorrer = kilometrosPorRecorrer;
		this.tipo=tipo;
	}

public String getTipo() {
	return tipo;
}

public void setTipo(String tipo) {
	this.tipo = tipo;
}

public abstract double pesoDeLacarga();

public void setIata(Iata iata) {
	Vuelo.iata = iata;
}
public Collection<Pasaje> getPasajes() {
	return pasajes;
}
public abstract double pesoDePasajeros ();

public int getCantidadDeAsientosComprados(){
	return this.pasajes.size();
}
public  abstract void pesoPermitidoDeEmpresas(double peso);

public double pesoMaximo(){
	return this.avion.getPesoDeAvion() + this.pesoDePasajeros() + this.pesoDeLacarga() + this.getPesoDeNafta() + Vuelo.iata.getPesoDeEQuipamiento() ;
}

public double getPesoDeNafta(){
	return this.kilometrosPorRecorrer * this.avion.getNafta();
}
public void agregarPasajes(Pasaje pasaje) {
	pasajes.add(pasaje);
	
}

public double getPrecioDeVenta(){
	return this.politica.precioDeVenta(this);
}

public abstract int getCantidadDeAsientosLibres() ;

public void setPrecioEstandar(double precioEstandar) {
	this.precioEstandar = precioEstandar;
}
public double getPrecioEstandar() {
	return precioEstandar;
}

public abstract int getCantidadDeAsientos() ;

public int getCantidadDeHientosDeAvion() {
	return this.avion.getCantidadDeAsientos();
}

public double pesoPermitidoDeEmpresa(){
	return this.pesoPermitidoDeLaEmpresa;
}
public void kilometrosPorRecorrer(double km){
	  this.kilometrosPorRecorrer = km;
	}
public boolean estaEstaPersona(int dni){
	return this.pasajes.stream().anyMatch(p->p.getDni()==dni);
}
public LocalDate getFecha(){
	return this.fechaDeVuelo;
}

public void setFechaDeVuelo(LocalDate fechaDeVuelo) {
	this.fechaDeVuelo = fechaDeVuelo;
}

public String getDestino() {
	return destino;
}

public void setEmpresa(Empresa empr) {
	this.empresa=empr;
	
}

public Empresa getEmpresa() {
	return empresa;
}



}
