package Vuelos;

import java.time.LocalDate;

public class VueloDeCarga extends Vuelo{

	public VueloDeCarga(String nombre, LocalDate fechaDeVuelo, String origen, String destino, 
			double kilometrosPorRecorrer, String tipo) {
		super(nombre, fechaDeVuelo, origen, destino, kilometrosPorRecorrer, tipo);
	}

	private double pesoDeEquipamiento= 700;
	private double pesoDeCarga;

	
	
	public String getNombre() {
		return nombre;
	}

	
	
	
	@Override
	public double pesoDeLacarga() {
		return this.pesoDeEquipamiento + this.pesoDeCarga;
	}
	@Override
	public double pesoDePasajeros() {
		return 0;
	}


	@Override
	public void pesoPermitidoDeEmpresas(double peso) {
		this.pesoPermitidoDeLaEmpresa=peso;
	}


	@Override
	public int getCantidadDeAsientos() {
		return 30;
	}




	@Override
	public int getCantidadDeAsientosLibres() {
	
		return this.avion.getCantidadDeAsientos() - this.getCantidadDeAsientosComprados(); 
	}
	

}
		