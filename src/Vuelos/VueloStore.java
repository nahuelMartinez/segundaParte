package Vuelos;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

import Aerolinea.Aerolines.Avion;
import Aerolinea.Aerolines.Pasaje;

import criterioEmpresa.CriterioDeVuelo;
import criterioEmpresa.Empresa;
import criterioEmpresa.Laxa;

import criterioEmpresa.Segura;
import politica.Estricta;
import politica.Politica;
import politica.Remate;

@Observable
public class VueloStore {

	public Collection<Vuelo> getVuelos() {
		return vuelos;
	}
	public void setVuelos(Collection<Vuelo> vuelos) {
		this.vuelos = vuelos;
	}
	private static VueloStore store = new VueloStore ();
	
	private Collection<Vuelo> vuelos;
	
	public VueloStore (){
		this.vuelos = new HashSet<>();
	}
	public static VueloStore store(){
		return store;
	}
	
	public void agregarVuelos(Vuelo vuelo){
		vuelos.add(vuelo);
	}
	
	public boolean estaEsteVuelo(Vuelo vuelo){
		return vuelos.contains(vuelo);
	}
	public int cantidaDeVuelos(){
	return 	vuelos.size();
	}
	
	
	
	
	public void iniciarVueloStore (){
		
	
			Avion Fonr = new Avion(30, 18, 21000,"Boeing 747-8");
			Fonr.setConsumoDeNaftaPorKilometro(300);
			
			VueloNormal Vuelo00 = new VueloNormal("vuelo01",LocalDate.of(2017, Month.DECEMBER,3) , "Buenos", "Mendoza"
					,300.300 ,"Normal");
			
			Politica estricta = new Estricta() ;
			
			CriterioDeVuelo seguro = new Laxa();
			
			Empresa nahuel = new Empresa();
		
			Pasaje pasaje1 = new  Pasaje("Sofi", 2084334, LocalDate.of(2017, Month.DECEMBER,3));
			Pasaje pasaje2 = new  Pasaje("nahuel", 20843539, LocalDate.of(2017, Month.AUGUST,22));
			Pasaje pasaje3 = new  Pasaje("Rosa", 20834354, LocalDate.of(2017, Month.FEBRUARY,6));
			Pasaje pasaje4 = new  Pasaje("ivan", 2543543, LocalDate.of(2017, Month.JUNE,22));
			Pasaje pasaje5 = new  Pasaje("fede", 243553443, LocalDate.of(2017, Month.AUGUST,29));
			
			Vuelo00.setAvion(Fonr);
			Vuelo00.setPrecioEstandar(100);
			Vuelo00.setPolitica(estricta);
			
			
			nahuel.setCriterio(seguro);
			nahuel.agregarVuelos(Vuelo00);
			nahuel.setCriteriaVuelo(Vuelo00);
			nahuel.setEmpresa(Vuelo00);
			
			nahuel.ventaDePasajes(Vuelo00, pasaje1);
			nahuel.ventaDePasajes(Vuelo00, pasaje2);
			nahuel.ventaDePasajes(Vuelo00, pasaje3);
			nahuel.ventaDePasajes(Vuelo00, pasaje4);
			nahuel.ventaDePasajes(Vuelo00, pasaje5);
			
			
			
			
			
			
			
			Avion Fonrr = new Avion(30, 18, 21000,"Boeing 747-9");
			
			Fonrr.setConsumoDeNaftaPorKilometro(609);
			
			VueloDeCarga Vuelo01 = new VueloDeCarga
					("vuelo02",LocalDate.of(2017, Month.DECEMBER,3) , "Buenos", "Mendoza"
					,300.300 ,"Carga");
			
			Politica estrict = new Estricta() ;
			
			CriterioDeVuelo Laxa = new Segura();
			
			Empresa argentina = new Empresa();
		
			Pasaje pasaje = new  Pasaje("Sofi", 2084334, LocalDate.of(2017, Month.DECEMBER,3));
			Pasaje pasaje01 = new  Pasaje("nahuel", 20843539, LocalDate.of(2017, Month.AUGUST,22));
			Pasaje pasaje02 = new  Pasaje("Rosa", 20834354, LocalDate.of(2017, Month.FEBRUARY,6));
			Pasaje pasaje03 = new  Pasaje("ivan", 2543543, LocalDate.of(2017, Month.JUNE,22));
			Pasaje pasaje04 = new  Pasaje("fede", 243553443, LocalDate.of(2017, Month.AUGUST,29));
			
			Vuelo01.setAvion(Fonrr);
			Vuelo01.setPrecioEstandar(100);
			Vuelo01.setPolitica(estrict);
			
			
			argentina.setCriterio(Laxa);
			argentina.agregarVuelos(Vuelo01);
			argentina.setCriteriaVuelo(Vuelo01);
			argentina.setEmpresa(Vuelo01);
			
			argentina.ventaDePasajes(Vuelo01, pasaje);
			argentina.ventaDePasajes(Vuelo01, pasaje01);
			argentina.ventaDePasajes(Vuelo01, pasaje02);
			argentina.ventaDePasajes(Vuelo01, pasaje03);
			argentina.ventaDePasajes(Vuelo01, pasaje04);
			
			
			
			Avion pirulo = new Avion(40, 18, 21000,"Boeing 747-10");
			
			pirulo.setConsumoDeNaftaPorKilometro(309);
			
			VueloCharter Vuelo02 = new VueloCharter("vuelo03",LocalDate.of(2017, Month.DECEMBER,3) , "Buenos", "Mendoza"
					,300.300,2,"charter");
			
			Politica remate = new Remate() ;
			
			CriterioDeVuelo segura = new Segura();
			
			Empresa chile = new Empresa();
		
			Pasaje pasaje00 = new  Pasaje("Sofi", 2084334, LocalDate.of(2017, Month.DECEMBER,3));
			Pasaje pasaje011 = new  Pasaje("nahuel", 20843539, LocalDate.of(2017, Month.AUGUST,22));
			Pasaje pasaje022 = new  Pasaje("Rosa", 20834354, LocalDate.of(2017, Month.FEBRUARY,6));
			Pasaje pasaje033 = new  Pasaje("ivan", 2543543, LocalDate.of(2017, Month.JUNE,22));
			Pasaje pasaje044 = new  Pasaje("fede", 243553443, LocalDate.of(2017, Month.AUGUST,29));
			
			Vuelo02.setAvion(pirulo);
			Vuelo02.setPrecioEstandar(100);
			Vuelo02.setPolitica(remate);
			
			
			chile.setCriterio(segura);
			chile.agregarVuelos(Vuelo02);
			chile.setCriteriaVuelo(Vuelo02);
			chile.setEmpresa(Vuelo02);
			
			chile.ventaDePasajes(Vuelo02, pasaje00);
			chile.ventaDePasajes(Vuelo02, pasaje011);
			chile.ventaDePasajes(Vuelo02, pasaje022);
			chile.ventaDePasajes(Vuelo02, pasaje033);
			chile.ventaDePasajes(Vuelo02, pasaje044);
			
			
			
			
			
			
			
			
			this.agregarVuelos(Vuelo00);
			this.agregarVuelos(Vuelo01);
			this.agregarVuelos(Vuelo02);
		
	}
	public LocalDate fechaDeVuelo(int dni,String destino){
		return this.vueloDePersonaBuscada(dni, destino).getFecha();
	}
	public List<Vuelo> destinoVuelo(String destino){
		return  store.vuelos.stream().filter(v->v.getDestino()== destino).collect(Collectors.toList());
	}
	public Vuelo vueloDePersonaBuscada(int dni,String destino){
		return destinoVuelo(destino).stream().filter(v->v.estaEstaPersona(dni)).findAny().get();
	}
}
