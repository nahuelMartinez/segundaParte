package criterioEmpresa;

import org.uqbar.commons.utils.Observable;

import Vuelos.Vuelo;

@Observable
public abstract class CriterioDeVuelo  {

	public abstract boolean puedeVender (Vuelo vuelo);
}
