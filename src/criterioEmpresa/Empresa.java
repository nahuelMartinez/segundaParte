package criterioEmpresa;

import java.util.Collection;
import java.util.HashSet;

import org.uqbar.commons.utils.Observable;

import Aerolinea.Aerolines.Pasaje;
import Vuelos.Vuelo;

@Observable
public class Empresa {
	
	public void setVuelos(Collection<Vuelo> vuelos) {
		this.vuelos = vuelos;
	}
	private double pesoPermitido;


	private  CriterioDeVuelo criterio;


	private Collection<Vuelo> vuelos;
	
	public Empresa() {
		this.vuelos = new HashSet<>();
	}
	
	public void setPesoPermitido(Vuelo vuelo , double peso){
		vuelo.pesoPermitidoDeEmpresas(peso);
	}
	public void ventaDePasajes (Vuelo vuelo,Pasaje pasaje){
		if(this.sePuedeVenderPasaje(vuelo)){
		vuelo.agregarPasajes(pasaje);
		pasaje.setPrecio(vuelo.getPrecioDeVenta());
		}
		else 
			throw new RuntimeException ("No se puedo Vender Pasaje");
	}
	
	
	
	
	
	public  void setCriterio(CriterioDeVuelo criterio) {
		this.criterio = criterio;
	}
	
	public  CriterioDeVuelo getCriterio() {
		return criterio;
	} 
	
	public boolean sePuedeVenderPasaje(Vuelo vuelo){
		return vueloEstaEnEmpresa(vuelo) && seCumpleCriterio(vuelo);
	}

	
	private boolean seCumpleCriterio(Vuelo vuelo) {
		return this.criterio.puedeVender(vuelo); 
	}

	public Collection<Vuelo> getVuelos() {
		return vuelos;
	}

	public void agregarVuelos(Vuelo vuelo) {
		this.vuelos.add(vuelo);
	}

	public boolean vueloEstaEnEmpresa(Vuelo Vuelo){
		return this.vuelos.contains(Vuelo);
	}
	public double getPesoPermitido() {
		return pesoPermitido;
	}
	public void setPesoPermitido(double pesoPermitido) {
		this.pesoPermitido = pesoPermitido;
	}
	public int cantidad(){
		return this.vuelos.size();
	}
	public void setCriteriaVuelo(Vuelo vuelo){
		vuelo.setCriterio(this.criterio);
	}
	public void setEmpresa(Vuelo vuelo){
		vuelo.setEmpresa(this);
	}
}
