package politica;



import Vuelos.Vuelo;

public class VentaAnticipada extends Politica {

	@Override
	public double precioDeVenta(Vuelo vuelo) {
		if(vuelo.getCantidadDeAsientosLibres() < 40){
			return   vuelo.getPrecioEstandar()* 0.80  ;
		}else if (vuelo.getCantidadDeAsientosLibres() > 40 && vuelo.getCantidadDeAsientosLibres() < 79){
		return vuelo.getPrecioEstandar() * 0.40 ;	
		}else
			return vuelo.getPrecioEstandar();
	}

}
