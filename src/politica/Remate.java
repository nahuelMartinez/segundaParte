package politica;

import Vuelos.Vuelo;

public  class Remate extends Politica {

	@Override
	public double precioDeVenta(Vuelo vuelo) {
		if (vuelo.getCantidadDeAsientosLibres() > 30 ){
			return  (vuelo.getPrecioEstandar() * (0.75 )) ;
		}else
			return vuelo.getPrecioEstandar() - vuelo.getPrecioEstandar() * 0.50 ;
	}

}
