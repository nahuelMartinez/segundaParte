package politica;

import org.uqbar.commons.utils.Observable;

import Vuelos.Vuelo;
@Observable
public abstract  class Politica {

	public abstract double precioDeVenta (Vuelo vuelo);
}
