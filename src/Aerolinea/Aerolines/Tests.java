package Aerolinea.Aerolines;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Test;

import Vuelos.VueloNormal;
import Vuelos.VueloStore;
import criterioEmpresa.AmenazaTerrorista;
import criterioEmpresa.CriterioDeVuelo;
import criterioEmpresa.Empresa;
import criterioEmpresa.Laxa;

import criterioEmpresa.Segura;
import politica.Estricta;
import politica.Politica;
import politica.Remate;
import politica.VentaAnticipada;

public class Tests {

	//1
	@Test
	public void CantidadDeHacientosLibres() {
		Avion avion = new Avion(100,3.30, 0);
		VueloNormal vuelo001 = new VueloNormal("vuelo101",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires", "Mendoza",499);
		vuelo001.setAvion(avion);
		Pasaje pasaje = new Pasaje("nahuel", 443322, LocalDate.of(2017, Month.AUGUST,22));
		vuelo001.agregarPasajes(pasaje);
	
		assertEquals(99, vuelo001.getCantidadDeAsientosLibres());
	}
	
	@Test
	public void CantidadDeHacientosLibres2() {
		Avion avion = new Avion(100,3.30, 0);
		VueloNormal vuelo001 = new VueloNormal("vuelo101",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires", "Mendoza",6000);
		vuelo001.setAvion(avion);
		Pasaje pasaje = new Pasaje("nahuel", 443322, LocalDate.of(2017, Month.AUGUST,12));
		Pasaje pasaje2 = new Pasaje("mario", 443322, LocalDate.of(2017, Month.AUGUST,30));
		Pasaje pasaje3 = new Pasaje("vale", 443322, LocalDate.of(2017, Month.AUGUST,10));
		Pasaje pasaje4 = new Pasaje("juan", 443322, LocalDate.of(2017, Month.AUGUST,9));
		vuelo001.agregarPasajes(pasaje);
		vuelo001.agregarPasajes(pasaje4);
		vuelo001.agregarPasajes(pasaje3);
		vuelo001.agregarPasajes(pasaje2);
	
		assertEquals(96, vuelo001.getCantidadDeAsientosLibres());
	}
	
	//2
	@Test
	public void EsRelajado(){
		Avion avion = new Avion (90,4.50, 0);
		VueloNormal vuelo002 = new  VueloNormal ("vuelo110",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires","Mendoza",5990);
		vuelo002.setAvion(avion);
		
		assertTrue(vuelo002.esRelajado());
	}
	@Test
	public void noesEsRelajado(){
		Avion avion = new Avion (100,1.30, 0);
		VueloNormal vuelo002 = new  VueloNormal ("vuelo101",LocalDate.of(2017, Month.AUGUST,22) ,"BuenosAires","Mendoza",3747);
		vuelo002.setAvion(avion);
		assertFalse(vuelo002.esRelajado());
	}
	
	//3
	@Test
	public void sePuedeVenderAVueloConAmenenazaTerrorista(){
		Avion avion = new Avion (100,1.30, 0);
		VueloNormal vuelo003 = new  VueloNormal ("vuelo100",LocalDate.of(2017, Month.AUGUST,22) ,"BuenosAires", "Mendoza",578.3);
		vuelo003.setAvion(avion);
		Empresa americanAirlines = new Empresa();
		americanAirlines.agregarVuelos(vuelo003);
		AmenazaTerrorista alertaRoja = new AmenazaTerrorista();
		americanAirlines.setCriterio(alertaRoja);
		
		assertEquals(1, americanAirlines.cantidad());
		assertFalse(americanAirlines.sePuedeVenderPasaje(vuelo003));
	}

	@Test
	public void sePuedeVenderAVueloSeguro(){
		Avion avion = new Avion (100,1.30, 0);
		VueloNormal vuelo004 = new  VueloNormal ("vuelo100",LocalDate.of(2017, Month.AUGUST,22) ,"BuenosAires","Mendoza",589.3);
		vuelo004.setAvion(avion);
		Empresa americanAi = new Empresa();
		americanAi.agregarVuelos(vuelo004);
		Segura seg = new Segura();
		americanAi.setCriterio(seg);
		
		assertTrue(americanAi.sePuedeVenderPasaje(vuelo004));
	}
	
	@Test
	public void noPuedeVender15PorLaPolitica(){
		Avion avion = new Avion (5,1.30, 0);
		VueloNormal vuelo004 = new  VueloNormal ("vuelo100",LocalDate.of(2017, Month.AUGUST,22) ,"BuenosAires","Mendoza",477);
		vuelo004.setAvion(avion);
		Politica estricta= new Estricta();
		vuelo004.setPolitica(estricta);
		Empresa americanAirlines = new Empresa();
		americanAirlines.agregarVuelos(vuelo004);
		
		Laxa seg = new Laxa();
		
		americanAirlines.setCriterio(seg);
		Pasaje pasaje = new Pasaje("nahuel", 443322, LocalDate.of(2017, Month.AUGUST,12));
		Pasaje pasaje2 = new Pasaje("mario", 443322, LocalDate.of(2017, Month.AUGUST,30));
		Pasaje pasaje3 = new Pasaje("vale", 443322, LocalDate.of(2017, Month.AUGUST,10));
		Pasaje pasaje4 = new Pasaje("juan", 443322, LocalDate.of(2017, Month.AUGUST,9));
		Pasaje pasaje5 = new Pasaje("mario", 443322, LocalDate.of(2017, Month.AUGUST,3));
		
		Pasaje pasaje6 = new Pasaje("nahuel", 443322, LocalDate.of(2017, Month.AUGUST,12));
		Pasaje pasaje7 = new Pasaje("mario", 443322, LocalDate.of(2017, Month.AUGUST,30));
		Pasaje pasaje8 = new Pasaje("vale", 443322, LocalDate.of(2017, Month.AUGUST,10));
		Pasaje pasaje9 = new Pasaje("juan", 443322, LocalDate.of(2017, Month.AUGUST,9));
		Pasaje pasaje10 = new Pasaje("mario", 443322, LocalDate.of(2017, Month.AUGUST,3));
		
		Pasaje pasaje11 = new Pasaje("nahuel", 443322, LocalDate.of(2017, Month.AUGUST,12));
		Pasaje pasaje12 = new Pasaje("mario", 443322, LocalDate.of(2017, Month.AUGUST,30));
		Pasaje pasaje13 = new Pasaje("vale", 443322, LocalDate.of(2017, Month.AUGUST,10));
		Pasaje pasaje14 = new Pasaje("juan", 443322, LocalDate.of(2017, Month.AUGUST,9));
		Pasaje pasaje15 = new Pasaje("mario", 443322, LocalDate.of(2017, Month.AUGUST,3));
		
		
		vuelo004.agregarPasajes(pasaje);
		vuelo004.agregarPasajes(pasaje4);
		vuelo004.agregarPasajes(pasaje3);
		vuelo004.agregarPasajes(pasaje2);
		vuelo004.agregarPasajes(pasaje5);
		
		vuelo004.agregarPasajes(pasaje6);
		vuelo004.agregarPasajes(pasaje7);
		vuelo004.agregarPasajes(pasaje8);
		vuelo004.agregarPasajes(pasaje9);
		vuelo004.agregarPasajes(pasaje10);
		vuelo004.agregarPasajes(pasaje11);
		
		vuelo004.agregarPasajes(pasaje14);
		vuelo004.agregarPasajes(pasaje13);
		vuelo004.agregarPasajes(pasaje12);
		americanAirlines.ventaDePasajes(vuelo004, pasaje15);
		
		
		assertFalse(americanAirlines.sePuedeVenderPasaje(vuelo004));
	}
	
	
	
	
	@Test
	public void preciodeVentaEstricta(){
		Avion avion2= new Avion(300, 4.00, 0);
		VueloNormal Vuelo339 = new VueloNormal("vuelo2",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires","Mendoza",589);
		Politica Estricta = new Estricta();
		Empresa America = new Empresa();
		Vuelo339.setPrecioEstandar(5.0);
		America.agregarVuelos(Vuelo339);
		Vuelo339.setAvion(avion2);
		Vuelo339.setPolitica(Estricta);
		assertEquals(5.0 , Vuelo339.getPrecioDeVenta(),0.00001);
		
	}
	

	
	@Test
	//la cantidad de acientos es mayor a 30 que es 50%//
	public void preciodeVentaRemate(){
		Avion avion2= new Avion(38, 4.00, 0);
		VueloNormal Vuelo339 = new VueloNormal("vuelo2",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires","Mendoza",588);
		Politica Remate = new Remate();
		Empresa America = new Empresa();
		
		Vuelo339.setPrecioEstandar(10);
		America.agregarVuelos(Vuelo339);
		Vuelo339.setAvion(avion2);
		Vuelo339.setPolitica(Remate);
		
		assertEquals(7.5 , Vuelo339.getPrecioDeVenta(),0.00001);
		
	}
	
	
	
	@Test
	//la cantidad de acientos es menor a 30 que es 25%//
	public void preciodeVentaRemate2 (){
		Avion avion2= new Avion(28, 4.00, 0);
		VueloNormal Vuelo339 = new VueloNormal("vuelo2",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires","Mendoza",637);
		Politica Remate = new Remate();
		Empresa America = new Empresa();
		
		Vuelo339.setPrecioEstandar(10);
		America.agregarVuelos(Vuelo339);
		Vuelo339.setAvion(avion2);
		Vuelo339.setPolitica(Remate);
		
		assertEquals(5.0 , Vuelo339.getPrecioDeVenta(),0.00001);
		
	}
	@Test
	// cantidad de acientos es mayor a 40 y 79
	public void preciodeVentaAnticipada(){
		Avion avion2= new Avion(55, 4.00, 0);
		VueloNormal Vuelo339 = new VueloNormal("vuelo2",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires","Mendoza",5363);
		Politica ventaA = new VentaAnticipada();
		Empresa America = new Empresa();
		Vuelo339.setPrecioEstandar(10.0);
		
		America.agregarVuelos(Vuelo339);
		Vuelo339.setAvion(avion2);
		Vuelo339.setPolitica(ventaA);

		
		assertEquals(4.0 , ventaA.precioDeVenta(Vuelo339),0.00001);
		
	}
	@Test
	//cantidad de acientos es menor 40
	public void preciodeVentaAnticipada2(){
		Avion avion2= new Avion(30, 4.00, 0);
		VueloNormal Vuelo339 = new VueloNormal("vuelo2",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires","Mendoza",1324);
		Politica ventaA = new VentaAnticipada();
		Empresa America = new Empresa();
		Vuelo339.setPrecioEstandar(10.0);
		
		America.agregarVuelos(Vuelo339);
		Vuelo339.setAvion(avion2);
		Vuelo339.setPolitica(ventaA);

		
		assertEquals(8.0 , ventaA.precioDeVenta(Vuelo339),0.00001);
		
	}
	
	@Test
	public void preciodeVenta1(){
		Avion avion2= new Avion(300, 4.00, 0);
		VueloNormal Vuelo339 = new VueloNormal("vuelo2",LocalDate.of(2017, Month.AUGUST,22) , "BuenosAires","Mendoza",3324);
		Politica VentaAnti = new Remate();
		Empresa America = new Empresa();
		Vuelo339.setPrecioEstandar(10);
		
		America.agregarVuelos(Vuelo339);
		Vuelo339.setAvion(avion2);
		Vuelo339.setPolitica(VentaAnti);

		
		assertEquals(7.5, Vuelo339.getPrecioDeVenta() ,0.00001);
		
	}
	@Test
	public void ventaDePasajeDeUnVuelo(){
		Pasaje pasaje =new Pasaje("Nahuel", 39449222, LocalDate.of(2017, Month.AUGUST,22));
		
		
		assertEquals(LocalDate.of(2017, Month.AUGUST,22), pasaje.getFecha());
	}
	@Test
	public void venderPasaje(){
		Avion Fonr = new Avion(120, 3, 1000);
		VueloNormal Vuelo00 = new VueloNormal("vuelo00",LocalDate.of(2017, Month.AUGUST,22) , "Buenos", "Mendoza",352);
		Politica estricta = new Estricta() ;
		CriterioDeVuelo seguro = new Segura();
		Empresa nahuel = new Empresa();
		
		Pasaje pasaje = new  Pasaje("Sofi", 2083839, LocalDate.of(2017, Month.AUGUST,22));
		
		Vuelo00.setAvion(Fonr);
		Vuelo00.setPrecioEstandar(100.60);
		Vuelo00.setPolitica(estricta);
		
		nahuel.setCriterio(seguro);
		nahuel.agregarVuelos(Vuelo00);
		nahuel.ventaDePasajes(Vuelo00, pasaje);
		assertEquals(1, Vuelo00.getCantidadDeAsientosComprados());
		assertTrue(Vuelo00.getPasajes().contains(pasaje));
		
	}
	@Test
	public void importeTotal(){
		Avion Fonr = new Avion(120, 3, 1000);
		VueloNormal Vuelo00 = new VueloNormal("vuelo00",LocalDate.of(2017, Month.AUGUST,22) , "Buenos", "Mendoza", 500.50);
		Politica estricta = new Estricta() ;
		CriterioDeVuelo seguro = new Segura();
		Empresa nahuel = new Empresa();
		
		Pasaje pasaje1 = new  Pasaje("Sofi", 2084334, LocalDate.of(2017, Month.DECEMBER,3));
		Pasaje pasaje2 = new  Pasaje("nahuel", 20843539, LocalDate.of(2017, Month.AUGUST,22));
		Pasaje pasaje3 = new  Pasaje("juan", 20834354, LocalDate.of(2017, Month.FEBRUARY,6));
		Pasaje pasaje4 = new  Pasaje("ivan", 2543543, LocalDate.of(2017, Month.JUNE,22));
		Pasaje pasaje5 = new  Pasaje("fede", 243553443, LocalDate.of(2017, Month.AUGUST,29));
		
		Vuelo00.setAvion(Fonr);
		Vuelo00.setPrecioEstandar(100.0);
		Vuelo00.setPolitica(estricta);
		
		
		nahuel.setCriterio(seguro);
		nahuel.agregarVuelos(Vuelo00);
		
		nahuel.ventaDePasajes(Vuelo00, pasaje1);
		nahuel.ventaDePasajes(Vuelo00, pasaje2);
		nahuel.ventaDePasajes(Vuelo00, pasaje3);
		nahuel.ventaDePasajes(Vuelo00, pasaje4);
		nahuel.ventaDePasajes(Vuelo00, pasaje5);
		
		
		assertEquals(500.0, Vuelo00.importeTotal() ,0.000001);
	
		
	}
	
	@Test 
	public void estaEsteVuelo(){
		VueloStore store = new VueloStore();
		
		store.iniciarVueloStore();
		VueloNormal Vuelo01 = new VueloNormal("vuelo00",LocalDate.of(2017, Month.DECEMBER,3 ), "Buenos", "Mendoza", 400.4);
		Pasaje pasaje1 = new  Pasaje("Sofi", 2084334, LocalDate.of(2017, Month.DECEMBER,3));
		Vuelo01.agregarPasajes(pasaje1);
		store.agregarVuelos(Vuelo01);
		assertEquals(2,store.cantidaDeVuelos());
		assertEquals(Vuelo01,store.vueloDePersonaBuscada(2084334,"Mendoza"));
		
	}
	
}
