package Aerolinea.Aerolines;

import java.time.LocalDate;

import org.uqbar.commons.utils.Observable;

@Observable
public class Pasaje {


private String nombre;
private int dni;
private LocalDate fecha;	
private double precio;


public double getPrecio() {
	return precio;
}


public void setPrecio(double precio) {
	this.precio = precio;
}
public Pasaje(){
	
}

public Pasaje (String nombre , int dni, LocalDate fecha){
	this.nombre= nombre;
	this.dni = dni;
	this.fecha=fecha;
}


public String getNombre() {
	return nombre;
}


public void setNombre(String nombre) {
	this.nombre = nombre;
}


public int getDni() {
	return dni;
}


public void setDni(int dni) {
	this.dni = dni;
}


public LocalDate getFecha() {
	return fecha;
}


public void setFecha(LocalDate fecha) {
	this.fecha = fecha;
}


}

