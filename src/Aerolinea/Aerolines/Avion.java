package Aerolinea.Aerolines;

import org.uqbar.commons.utils.Observable;

@Observable
public class Avion {
	
	private int cantidadDeAsientos;
	private double alturaCabina;
	private double pesoDeAvion;
	private double consumoDeNaftaPorKilometro;
	private String modelo;
	
	
	public double getPesoDeAvion() {
		return pesoDeAvion;
	}

	public Avion(int cantidadDeAsientos,double cabina,int peso,String modelo) {
		super();
		this.modelo=modelo;
		this.cantidadDeAsientos = cantidadDeAsientos;
		this.alturaCabina = cabina;
		this.pesoDeAvion=peso;
	}



	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public double getAlturaCabina() {
		return alturaCabina;
	}

	public double getConsumoDeNaftaPorKilometro() {
		return consumoDeNaftaPorKilometro;
	}

	public void setConsumoDeNaftaPorKilometro(double consumoDeNaftaPorKilometro) {
		this.consumoDeNaftaPorKilometro = consumoDeNaftaPorKilometro;
	}

	public void setCantidadDeAsientos(int cantidadDeAsientos) {
		this.cantidadDeAsientos = cantidadDeAsientos;
	}

	public void setPesoDeAvion(double pesoDeAvion) {
		this.pesoDeAvion = pesoDeAvion;
	}

	public void setAlturaCabina(double alturaCabina) {
		this.alturaCabina = alturaCabina;
	}

	public int getCantidadDeAsientos() {
		return this.cantidadDeAsientos;
	}
	
	public double getNafta(){
		return  this.consumoDeNaftaPorKilometro;
	}
}
